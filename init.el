;;; init.el --- Initial script to load temacco  -*- lexical-binding: t; -*-

;; Copyright (C) 2011-2024  Toon Claes

;; Author: Toon Claes
;; URL: https://gitlab.com/to1ne/temacco
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains the minimum to load the literate programming
;; configuration for temacco.

;;; Code:

;; Root directory

;; Define variable for the configuration root directory.


;; [[file:README.org::*Root directory][Root directory:1]]
(defvar temacco-root-dir
  user-emacs-directory
  "Root directory of the temacco configuration (probably ~/.config/emacs.d/).")
;; Root directory:1 ends here

;; Core file

;; Define variable for the filename of the core configuration file.

;; [[file:README.org::*Core file][Core file:1]]
(defvar temacco-core-file
  (expand-file-name "README.org" temacco-root-dir)
  "Core of the temacco configuration.")
;; Core file:1 ends here

;; Load Org

;; Ensure org-mode and org-babel are loaded.


;; [[file:README.org::*Load Org][Load Org:1]]
(require 'org)
(require 'ob-tangle)
;; Load Org:1 ends here

;; Ignite

;; Ignite the org-babel magic.


;; [[file:README.org::*Ignite][Ignite:1]]
(org-babel-load-file temacco-core-file)
;; Ignite:1 ends here

;; Wrap up

;; Some footer shenanigans.


;; [[file:README.org::*Wrap up][Wrap up:1]]
(provide 'init)
;;; init.el ends here
;; Wrap up:1 ends here
