;;; rate-master.el --- Rates every line.             -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Toon Claes

;; Author: Toon Claes <toon@iotcl.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Put a rating on each line.
;;
;; Press a number from 0 to 5 to give the current line a rating.  It
;; sets the rating by prepending the line with stars.
;;
;;  - "☆☆☆☆☆" for rating 0
;;  - "★☆☆☆☆" for rating 1
;;  - "★★☆☆☆" for rating 2
;;  - "★★★☆☆" for rating 3
;;  - "★★★★☆" for rating 4
;;  - "★★★★★" for rating 5
;;
;; It overwrites previous rating, if there was one.  Assigning a
;; rating, moves point to the next line.
;;
;; Press - (minus) to remove the rating from this line, or the
;; previous.

;;; Code:

(defun rate-master--stars (value)
  "Convert VALUE to stars."
  (concat (make-string value ?★)
          (make-string (- 5 value) ?☆)
          " "))

(defun rate-master--unrate-line ()
  "Unrate the current line."
  (save-excursion
    (beginning-of-line)
    (when (looking-at-p "[★☆]\\{5\\} ")
        (zap-to-char 1 ? ) t)))

(defun rate-master--rate (value)
  "Give rating of VALUE."
  (save-excursion
    (rate-master--unrate-line)
    (beginning-of-line)
    (insert (rate-master--stars value)))
  (next-line)
  (recenter))

(defun rate-master--rate-0 ()
  "Give rating of 0."
  (interactive "*")
  (rate-master--rate 0))

(defun rate-master--rate-1 ()
  "Give rating of 1."
  (interactive "*")
  (rate-master--rate 1))

(defun rate-master--rate-2 ()
  "Give rating of 2."
  (interactive "*")
  (rate-master--rate 2))

(defun rate-master--rate-3 ()
  "Give rating of 3."
  (interactive "*")
  (rate-master--rate 3))

(defun rate-master--rate-4 ()
  "Give rating of 4."
  (interactive "*")
  (rate-master--rate 4))

(defun rate-master--rate-5 ()
  "Give rating of 5."
  (interactive "*")
  (rate-master--rate 5))

(defun rate-master--back ()
  "Remove the rating from previous item."
  (interactive "*")
  (unless (rate-master--unrate-line)
    (forward-line -1)
    (rate-master--unrate-line)))

(defvar rate-master-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "0") 'rate-master--rate-0)
    (define-key map (kbd "1") 'rate-master--rate-1)
    (define-key map (kbd "2") 'rate-master--rate-2)
    (define-key map (kbd "3") 'rate-master--rate-3)
    (define-key map (kbd "4") 'rate-master--rate-4)
    (define-key map (kbd "5") 'rate-master--rate-5)
    (define-key map (kbd "-") 'rate-master--back)
    map)
  "A keymap for the rate master keybindings.")

(defcustom rate-master-mode-line " Rate Master"
  "Lighter for `rate-master-mode'."
  :type 'string
  :group 'rate-master)

(define-minor-mode rate-master-mode
  "Minor mode to rate every line in file."
  t
  rate-master-mode-line
  rate-master-mode-map)

(provide 'rate-master)
;;; rate-master.el ends here
