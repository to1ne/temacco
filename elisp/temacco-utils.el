;;; temacco-utils.el --- Various util functions      -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Toon Claes

;; Author: Toon Claes <toon@iotcl.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'hl-line)
(require 'eww)

(defun temacco-hide-cursor ()
  "Hide the cursor and current line."
  (interactive)
  (make-variable-buffer-local 'cursor-type)
  (make-variable-buffer-local 'global-hl-line-mode)
  (setq cursor-type         'bar
        global-hl-line-mode nil)
  (hl-line-unhighlight))

(defun temacco-lire (url)
  "Visit URL and bypass cookie notice and paywall."
  (interactive "sURL: ")
  (let ((url-user-agent "Slackbot-LinkExpanding 1.0 (+https://api.slack.com/robots)"))
    (eww-browse-url url)))

(provide 'temacco-utils)
;;; temacco-utils.el ends here
